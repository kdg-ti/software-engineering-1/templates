

Basis Resources : https://plantuml.com/
Crash Course : https://crashedmind.github.io/PlantUMLHitchhikersGuide/index.html

# Some commands

##don't show circles
``` 
hide circle 
```

##don't show methods [only when empty]
```
hide [empty] methods
```

##constant width
see also https://forum.plantuml.net/1296/constant-width-of-class  ```   |
```
skinparam minClassWidth 70
 'OF 
skinparam sameClassWidth true 
 ```


## Change background colour for ALL classes
```
skinparam classBackgroundColor Wheat
 'OF
skinparam class{
   BackgroundColor Green
}
‘ OF
class Student #Purple{
    name
}
```


