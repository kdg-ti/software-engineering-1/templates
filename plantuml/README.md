# PLANTUML examples

`kdg-style.iuml` : template to display plantuml according to kdg style

to use this template the following code can be used.

_uncomment the include line that you want_


    /'use when template file is local '/
    '!include SE1-plantuml-template.iuml

    /'use when template file is remote '/
    '!include https://gitlab.com/kdg-ti/software-engineering-1/templates/-/raw/main/plantuml/kdg-style.iuml


See `
* `test-class.puml` for an example class diagram
* `test-sequence.puml` for an example sequence diagram 

