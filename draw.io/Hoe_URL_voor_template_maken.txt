Originele URL van voorbeeld:

https://app.diagrams.net/?libs=0&clibs=Uhttps%3A%2F%2Fjgraph.github.io%2Fdrawio-libs%2Flibs%2Ftemplates.xml

GitLab URL:

https://gitlab.com/gilles.oorts/diagrams.net-templates/-/blob/main/SE1-domeinmodel_library.xml
https://gitlab.com/gilles.oorts/diagrams.net-templates/-/blob/main/SE1_objectdiagram_library.xml
https://gitlab.com/gilles.oorts/diagrams.net-templates/-/blob/main/SE1_UCdiagram_library.xml

GitLab URL RAW:

https://gitlab.com/gilles.oorts/diagrams.net-templates/-/raw/main/SE1-domeinmodel_library.xml
https://gitlab.com/gilles.oorts/diagrams.net-templates/-/raw/main/SE1_objectdiagram_library.xml
https://gitlab.com/gilles.oorts/diagrams.net-templates/-/raw/main/SE1_UCdiagram_library.xml

ONZE URL:

https://app.diagrams.net/?libs=0&clibs=Uhttps://gitlab.com/gilles.oorts/diagrams.net-templates/-/raw/main/SE1-domeinmodel_library.xml;Uhttps://gitlab.com/gilles.oorts/diagrams.net-templates/-/raw/main/SE1_objectdiagram_library.xml;Uhttps://gitlab.com/gilles.oorts/diagrams.net-templates/-/raw/main/SE1_UCdiagram_library.xml



