# SE Templates
This project hosts all template files used in Software Engineering 1 course

## Some other sources of template material 
Applicatie : Lucidchart

Cases : Analyse & Design
https://lucid.app/lucidchart/7fb21946-a93d-4e26-9461-e9dc84b82bde/edit?page=ApqcoePoWWD4#?folder_id=home&browser=list

Modeloplossingen Design : Werkdocument
https://lucid.app/lucidchart/1357e2f7-34c1-4940-9695-0caa04734925/edit?page=0-SabE~Jeijj#?folder_id=search&browser=list

Cases : Examen Analyse 20-21
https://lucid.app/lucidchart/2de8f10b-03f9-4d93-973b-5bb5b409634c/edit?page=jiQ8Vf_z8dRP#?folder_id=home&browser=list 


